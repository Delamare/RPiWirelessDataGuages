#include "Stepper.h"
#include <iostream>
#include <thread>
#include <stdlib.h>

int main()
{
	std::cout << "Test of sanity!\n";
	wiringPiSetupGpio();
	// Stepper stepper(21, 20, 16, 12);
	Stepper stepper(26, 19, 13, 6);
	stepper.speed = 2;
	stepper.Clear();
	stepper.MoveCounterClockwise(stepper.maxSteps);
	stepper.MoveClockwise(stepper.maxSteps);
	delay(10);
	return 0;
}
