#include <iostream>
#include <thread>
#include <fstream>
#include <stdlib.h>
#include <unistd.h>
#include <string>
#include <sstream>
#include "Stepper.h"

void StartNetcat()
{
	system("nc -l 4321 > data.txt");
}

// Test
void WriteLine(const char* message)
{
	std::cout << message;
}

int main()
{
	// Stepper steppers[2];
	std::thread nc(StartNetcat);
	std::cout << "\nCreating threads";
	std::thread threads[2];
	Stepper steppers[2];
	wiringPiSetupGpio();
	steppers[0] = Stepper(21, 20, 16, 12);
	steppers[0].speed = 2;
	steppers[0].Clear();
	steppers[0].MoveCounterClockwise(steppers[0].maxSteps);

	steppers[1] = Stepper(26, 19, 13, 6);
	steppers[1].speed = 2;
	steppers[1].Clear();
	steppers[1].MoveCounterClockwise(steppers[1].maxSteps);

	std::ifstream file;
	for(;;)
	{
		file.open("data.txt", std::ifstream::in);
		if(file.good() && file.peek() != std::ifstream::traits_type::eof())
		{
			std::string line;
			while(std::getline(file, line))
			{
				unsigned int number, value;
				std::istringstream iss(line);
				if(!(iss >> number >> value) || number > 2)
					break;
				std::cout << "\nData is {" << number << "}{" << value << "}\n{" << steppers[number - 1].currentStep << "}{" << steppers[number - 1].maxSteps << "}";
				// threads[number - 1] = std::thread(WriteLine, "\nhello\n"); // Test
				// std::cout << "\nif " << value << " > " << (stepper.currentStep / stepper.maxSteps) * 100 << "\n";
				if(value > ((float)steppers[number - 1].currentStep / (float)steppers[number - 1].maxSteps) * (float)100)
				{
					threads[number - 1] = std::thread(&Stepper::MoveClockwise, steppers[number - 1], (((float)value / (float)100) * steppers[number - 1].maxSteps) - (float)steppers[number - 1].currentStep);
					steppers[number - 1].currentStep += (((float)value / (float)100) * steppers[number - 1].maxSteps) - (float)steppers[number - 1].currentStep;
				}
				else
				{
					threads[number - 1] = std::thread(&Stepper::MoveCounterClockwise, steppers[number - 1], (float)steppers[number - 1].currentStep - (((float)value / (float)100) * steppers[number - 1].maxSteps));
					steppers[number - 1].currentStep -= (float)steppers[number - 1].currentStep - (((float)value / (float)100) * steppers[number - 1].maxSteps);
				}
			}
			system("rm data.txt");
			if(nc.joinable())
				nc.join();
			nc = std::thread(StartNetcat);

			for(int i = 0; i < 2; i++)
				if(threads[i].joinable())
					threads[i].join();
			std::cout << "\nThreads joined\n";
		}
		file.close();
		usleep(250000);
	}
	system("killall nc");
	nc.join();
}
