#include "Stepper.h"

Stepper::Stepper()
{
	speed = 1;
	maxSteps = 160;
	currentStep = 0;
}

Stepper::Stepper(int A1, int A2, int B1, int B2)
{
	speed = 2;
	maxSteps = 160;
	currentStep = 0;
	this->A1 = A1;
	this->A2 = A2;
	this->B1 = B1;
	this->B2 = B2;
	pinMode(A1, OUTPUT);
	pinMode(A2, OUTPUT);
	pinMode(B1, OUTPUT);
	pinMode(B2, OUTPUT);
}

void Stepper::SetStep(bool a1, bool a2, bool b1, bool b2)
{
	digitalWrite(A1, a1);
	digitalWrite(A2, a2);
	digitalWrite(B1, b1);
	digitalWrite(B2, b2);
}

void Stepper::Clear()
{
	SetStep(0,0,0,0);
}

void Stepper::MoveCounterClockwise(int steps)
{
	for(int i=0; i < steps; i++)
	{
		SetStep(1,0,1,0);
		delay(speed);
		SetStep(0,1,1,0);
		delay(speed);
		SetStep(0,1,0,1);
		delay(speed);
		SetStep(1,0,0,1);
		delay(speed);
	}
}


void Stepper::MoveClockwise(int steps)
{
	for(int i=0; i < steps; i++)
	{
		SetStep(1,0,0,1);
		delay(speed);
		SetStep(0,1,0,1);
		delay(speed);
		SetStep(0,1,1,0);
		delay(speed);
		SetStep(1,0,1,0);
		delay(speed);
	}
}
