#include <iostream>
#include <fstream>
#include <unistd.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>

#define GETMEM "free | grep Mem | awk '{usage=(($2-$4)/$2)*100} END {print usage}'"
#define GETCPU "sar -u 1 1 | grep all | awk '{usage=$3} END {print usage}'"

// Returns the output of a command
std::string exec(char* cmd)
{
    FILE* pipe = popen(cmd, "r");
    if (!pipe) return "ERROR";
    char buffer[128];
    std::string result = "";
    while(!feof(pipe))
    	if(fgets(buffer, 128, pipe) != NULL)
    		result += buffer;
    pclose(pipe);
    return result;
}

// Show basic uasge
void ShowHelp()
{
	std::cout << "\nUsage:\nPIGuage <IP Address> <update speed in seconds> <WhatToShowOnGuage1> [<WhatToShowOnGuage2>]\n";
	std::cout << "Acceptable things to show:\n";
	std::cout << "cpu\n";
	std::cout << "memory\n";
	std::cout << "<path to command>\n";
	std::cout << "\n";
}

// Check for valid argument list
bool ValidArgs(int argc, char* argv[])
{
	if(argc < 4 || argv[1] == "-h" || argv[1] == "--help")
	{
		ShowHelp();
		return false;
	}
	return true;
}

// Check if the custom command exists
std::string GetCustomCommand(std::string path)
{
	std::ifstream commandFile(path.c_str());
	if(!commandFile.good())
	{
		std::cout << path << " not found.\n";
		return "";
	}
	else
		return path;
}

int main(int argc, char* argv[])
{
	// Checks and variable setup
	if(!ValidArgs(argc, argv))
		return 1;
	std::string command1 = "";
	std::string command2 = "";
	std::string result1 = "";
	std::string result2 = "";
	float waitTime = 1000000 * atof(argv[2]);

	// Set the IP address
	std::string ip = argv[1];

	// Setup commands
	if(std::string(argv[3]) == "cpu")
		command1 = GETCPU;
	else if(std::string(argv[3]) == "memory")
		command1 = GETMEM;
	else
	{
		command1 = GetCustomCommand(argv[3]);
		if(command1 == "")
			return 1;
	}
	if(argc == 5) // If Guage 2 is to be used
	{
		if(std::string(argv[4]) == "cpu")
			command2 = GETCPU;
		if(std::string(argv[4]) == "memory")
			command2 = GETMEM;
		else
		{
			command2 = GetCustomCommand(argv[4]);
			if(command2 == "")
				return 1;
		}
	}
	// Main run loop
	for(;;)
	{
		// Get data
		result1 = exec((char*)command1.c_str());
		result2 = exec((char*)command2.c_str());

		// Log the data
		std::cout << result1 << "\n";
		std::cout << result2 << "\n";

		std::ofstream outputFile;
		outputFile.open("data.txt");
		if(result1 != "")
			outputFile << "1 " << result1;
		if(result2 != "")
			outputFile << "2 " << result2;
		outputFile.close();

		// Send the data
		if(result1 != "")
			system(std::string("cat data.txt | nc " + std::string(argv[1]) + " 4321").c_str());

		// Sleep
		usleep(waitTime);
	}
}
