#pragma once

#include <wiringPi.h>

class Stepper
{
public:
	float speed;
	unsigned int currentStep, maxSteps, A1, A2, B1, B2;

	Stepper(int A1, int A2, int B1, int B2);

	void MoveCounterClockwise(int steps);
	void MoveClockwise(int steps);
	void Clear();
private:
	Stepper();
	void SetStep(bool a1, bool a2, bool a3, bool a4);
};
